    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">
    $(function() {
	escapeTag = function(string) {
	    if (string == null) return string;
	    return string.replace(/[&<>"']/g,

				  function(match) {
				      return {
					  '&' : '&amp;',
					  '<' : '&lt;',
					  '>' : '&gt;',
					  '"' : '&quot;',
					  "'" : '#&39;'
				      }[match];
				  });
	};

	var city = '130010'; // Tokyo
	city = '080020';    // Matsuyama ← 松山で上書き
	var wetherURL = 'http://weather.livedoor.com/forecast/webservice/json/v1?city='+city;
	$.getJSON('http://pipes.yahoo.com/pipes/pipe.run?u='+encodeURI(wetherURL)+'&_id=332d9216d8910ba39e6c2577fd321a6a&_render=json&_callback=?',
		  {},
		  function(json) {
		      var item = json.value.items[0];
		      $('<div><b>'
			+ escapeTag(item.location.city)
			+ escapeTag(item.forecasts[0].dateLabel)
			+ '</b> <img src="+escapeTag(item.forecasts[0].image.url)+">'
			+ ' <small>'
			+ escapeTag(item.forecasts[0].telop)
			+ '</small> <small>copyright livedoor 天気情報</small>'
			+ '</div>'
		       ).appendTo('#report');
		  });
    });
        </script>
