class DiariesController < ApplicationController
  before_action :set_diary, only: [:show, :edit, :update, :destroy]
  before_filter :auth, :only => ['new','edit','create','destroy']
  # GET /diaries
  # GET /diaries.json
  def index
    @diaries = Diary.all
    #my
    #@test_form= TestForm.new params[:search_cond]
    #@test = @diaries.search
    #my
  end

  # GET /diaries/1
  # GET /diaries/1.json
  def show
  end

  # GET /diaries/new
  def new
    @diary = Diary.new
  end

  # GET /diaries/1/edit
  def edit
  end

  # POST /diaries
  # POST /diaries.json
  def create
    @diary = Diary.new(diary_params)

    respond_to do |format|
      if @diary.save
        format.html { redirect_to @diary, notice: 'Diary was successfully created.' }
        format.json { render :show, status: :created, location: @diary }
      else
        format.html { render :new }
        format.json { render json: @diary.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /diaries/1
  # PATCH/PUT /diaries/1.json
  def update
    respond_to do |format|
      if @diary.update(diary_params)
        format.html { redirect_to @diary, notice: 'Diary was successfully updated.' }
        format.json { render :show, status: :ok, location: @diary }
      else
        format.html { render :edit }
        format.json { render json: @diary.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /diaries/1
  # DELETE /diaries/1.json
  def destroy
    @diary.destroy
    respond_to do |format|
      format.html { redirect_to diaries_url, notice: 'Diary was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  

  #my change point
 

  
  def search
       @cond =params[:search_cond]
       @diaries= Diary.where('body like ? or title like? or date like?','%' + @cond + '%','%' + @cond + '%','%' + @cond + '%')
        respond_to do |format|
        format.html
        format.json { render json: @diaries}
      
        end
end         
   


  
             


  
  # end my change point search
  

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_diary
      @diary = Diary.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def diary_params
      params.require(:diary).permit(:date, :title, :body)
    end
end

private

=begin
def auth
  authenticate_or_request_with_http_digest('MyDiary') do |name|
    u = User.find_by_name(name)
    if u
      u.password
    else
      nil
    end
  end
end
=end

#my change code
def auth                                                                                                                                                                                                     realm ='MyDiary'
  authenticate_or_request_with_http_digest(realm) do |name|
    pass = nil
    open('./ht-digest-password'){|io|
      while line = io.gets
        nrp = line.chomp.split(":",3)
        if nrp[1] != realm
          next
        elsif nrp[0] == name
          pass = nrp[2]
          break
        end                                                                                    
      end                                                                                                                                                                                                         }
    pass
    end                                                                                                                                                                                                     
  end                                                                                                                                                                                                       
#end my change code
