# coding: utf-8
class Diary < ActiveRecord::Base

  #my change point
  validates_presence_of :title #title not null

  validate :date_isvalid? #check diary date

  private
  def date_isvalid?
    if date < (DateTime.now -1)
      errors.add(:date, '1日以上前の日記を作成・修正することはできません。')
    end
  end
  # end my change point  
end

#test 


